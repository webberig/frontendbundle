<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mathieu
 * Date: 28/09/13
 * Time: 11:15
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\FrontendBundle\Service;

use Knp\Menu\MenuFactory;
use Webberig\CMSBundle\Entity\Menuitem;

class MenuBuilder {
    private $options;
    /**
     * @var \Webberig\CMSBundle\Service\Menu
     */
    private $menuService;

    private $activeMenus;
    public function __construct($menuService) {
        $this->options = array(
            'group' => 1,
        );
        $this->menuService = $menuService;
    }

    public function setOptions($options) {
        $this->options = array_merge($this->options, $options);
    }

    public function build() {
        if (!isset($this->options["language"])) {
            throw new \Exception("Missing parameter: language");
        }
        $this->activeMenus = array();
        if (isset($this->options["page"])) {
            $this->activeMenus = $this->menuService->getActiveMenuIds($this->options["page"]);
        }

        $factory = new MenuFactory();
        $knp = $factory->createItem("menu");

        $menuItems = $this->menuService->getMenuItems($this->options["group"], $this->options["language"]);
        $this->fillMenu($knp, $menuItems);

        return $knp;
    }

    /**
     * @param $knp \Knp\Menu\MenuItem
     * @param $menu
     */
    private function fillMenu($knp, $menu) {
        /**
         * @var $m Menuitem
         */
        foreach ($menu as $m) {
            $item = $knp->addChild($m->getId());
            $item->setLabel($m->getName());
            $item->setUri($m->getUrl());
            $item->setExtra("pageID", $m->getPageId());

            if (isset($this->options["page"]) && $this->options["page"]->getId() == $m->getPageId()) {
                $item->setCurrent(true);
            }

            if ($m->getSubmenus()->count()) {
                $this->fillMenu($item, $m->getSubmenus());
            }

        }
    }



}