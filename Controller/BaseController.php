<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Mathieu
 * Date: 15/08/13
 * Time: 17:21
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\FrontendBundle\Controller;

use Webberig\CMSBundle\Entity\Language;

class BaseController  extends \Webberig\CMSBundle\Controller\BaseController {
    /**
     * @param null $abbr
     * @return null|Language
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function getLanguage($abbr = null)
    {
        if ($abbr === null) {
            $abbr = $this->getRequest()->getLocale();
        }
        $languageService = $this->get('CMS.language');
        $l = $languageService->getByAbbreviation($abbr);
        if (!$l)
            throw $this->createNotFoundException('Page not found  - Unknown language');

        return $l;

    }

}