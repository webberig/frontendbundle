<?php

namespace Webberig\FrontendBundle\Controller;

class DefaultController extends BaseController
{
    public function indexAction()
    {
        $l = $this->getLanguage();
        $page = $this->getPageService()->getBySlug("home", $l->getId());

        if (!$page)
            throw $this->createNotFoundException('Page not found');

        return $this->render('WebberigFrontendBundle:Default:index.html.twig', array('page' => $page));
    }

    public function languageAction()
    {
        $languageService = $this->get('CMS.language');
        $l = $languageService->getFirst();
        if (!$l)
            throw $this->createNotFoundException('Page not found - Unknown language');

        return $this->redirect($this->generateUrl('webberig_frontend_homepage', array()), 301);
    }

    public function pageAction($page)
    {
        $l = $this->getLanguage();
        $p = $this->getPageService()->getBySlug($page, $l->getId());

        if (!$p)
            throw $this->createNotFoundException('Page not found');

        return $this->render('WebberigFrontendBundle:Default:page.html.twig', array('page' => $p));
    }

    public function itemAction($category, $item)
    {
        $l = $this->getLanguage();
        $c = $this->getCategoryService()->getByUrl($category);
        $p = $this->getPageService()->getBySlug($item, $l->getId(), $c->getId());

        if (!$p)
            throw $this->createNotFoundException('Page not found');

        if ($c->getUseGallery()) {
            $gallery = $this->getPageService()->getGallery($p);
        } else {
            $gallery = null;
        }

        return $this->render('WebberigFrontendBundle:Default:page.html.twig', array(
            'page' => $p,
            'gallery' => $gallery
        ));
    }
}
