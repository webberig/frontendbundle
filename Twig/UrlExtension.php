<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 14/02/13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\FrontendBundle\Twig;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;

class UrlExtension extends \Twig_Extension
{
    /****************************************************************************************************************
     * Utilities
     ****************************************************************************************************************/
    private $env;
    private $container;

    /**
     * @return Request
     */
    private function getRequest() {
        return $this->container->get("request");
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $this->env = $environment;
    }

    public function getName()
    {
        return 'url_extension';
    }

    public function __construct(Container $container) {
        $this->container = $container;
    }

    public function getFilters() {
        return array(
            "url" => new \Twig_Filter_Method($this, "getUrl"),
        );
    }

    public function getUrl($path, $absolute = false)
    {
        if (substr(strtolower($path),0,4) == "http") {
            return $path;
        }
        if (substr($path,0,1) != "/") {
            $path = "/" . $path;
        }
        if ($absolute) {
            return $this->getRequest()->getUriForPath($path);
        } else {
            return $this->getRequest()->getBaseUrl() . $path;
        }
    }

}