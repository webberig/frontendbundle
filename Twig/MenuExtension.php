<?php
/**
 * Created by JetBrains PhpStorm.
 * User: mathieu
 * Date: 14/02/13
 * Time: 21:46
 * To change this template use File | Settings | File Templates.
 */

namespace Webberig\FrontendBundle\Twig;

use Knp\Menu\Matcher\Matcher;
use Knp\Menu\MenuFactory;
use Webberig\FrontendBundle\Service\MenuBuilder;

class MenuExtension extends \Twig_Extension
{
    /****************************************************************************************************************
     * Utilities
     ****************************************************************************************************************/
    private $env;
    /**
     * @var MenuBuilder
     */
    private $menuBuilder;


    public function initRuntime(\Twig_Environment $environment)
    {
        $this->env = $environment;
    }

    public function getName()
    {
        return 'menu_extension';
    }

    public function __construct(MenuBuilder $menuBuilder) {
        $this->menuBuilder = $menuBuilder;
    }

    public function getFunctions() {
        return array(
            "renderMenu" => new \Twig_Function_Method($this, "renderMenu", array('is_safe' => array('html'))),
            "getMenu" => new \Twig_Function_Method($this, "getMenu"),
        );
    }

    private function buildMenu($options)
    {
        $this->menuBuilder->setOptions($options);
        return $this->menuBuilder->build();
    }


    /****************************************************************************************************************
     * Functions
     ****************************************************************************************************************/
    public function getMenu($options)
    {
        return $this->buildMenu($options);
    }
    public function renderMenu($view, $options)
    {
        $menu = $this->buildMenu($options);
        return $this->env->render($view, array("menu" => $menu, "options" => $options, "matcher" => new Matcher()));
    }
}